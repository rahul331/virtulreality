#include "bullet.h"

void Bullet::localSimulate(double dt){

    GMlib::Vector<float,3> g(0,0,-9.81);
    vel+=dt*g;
    vel+=this->getMatrixToSceneInverse()*g *dt ;

    this->translate(vel);


}
