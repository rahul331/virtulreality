
varying vec4 texCoord;


void main()
{
    texCoord = gl_Vertex;
    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;

}
	
	