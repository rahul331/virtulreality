#ifndef BULLET_H
#define BULLET_H

#include <memory>
#include <parametrics/gmpsphere>
#include <parametrics/gmpbeziersurf>
#include <iostream>
#include <QTimerEvent>
#include <QRectF>
#include <QMouseEvent>
#include <QDebug>
#include <stdexcept>
#include <thread>
#include <mutex>

class Bullet : public GMlib::PSphere<float>
{
public:
   using PSphere::PSphere;
    GMlib::Vector<float, 3> g;
    GMlib::Vector<float, 3>  vel=GMlib::Vector<float,3>(.1,-1,-1);

    ~Bullet() {}





protected:
    void localSimulate(double dt);

};
#endif // BULLET_H


