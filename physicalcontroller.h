/*
 this is the most importaint class
 _surf: define the plane
 _ballArray: define the array which containt the ball
 _planeArray: define the array which containt the wall
 _collisionArray: store the data of the collision
 */

#ifndef PHYSICALCONTROLLER_H
#define PHYSICALCONTROLLER_H

#include "plane.h"
#include "bullet.h"
#include "vizulizer.h"
#include"geometryshader.h"


#include <parametrics/gmpsphere>
#include <core/gmarray>
#include <QTimerEvent>
#include <QRectF>
#include <QMouseEvent>
#include <QDebug>
#include <stdexcept>
#include <thread>
#include <mutex>

#include <gmOpenglModule>
#include <gmSceneModule>
#include <gmParametricsModule>
#include <parametrics/gmpsphere>
#include <parametrics/gmpplane>
#include <parametrics/gmpcylinder>
#include <parametrics/gmpcircle>
// Qt library
#include <QTimerEvent>
#include <QRectF>
#include <QMouseEvent>
#include <QDebug>
// stl library
#include <stdexcept>
#include <thread>
#include <mutex>


class PhysicalController : public GMlib::PSphere<float>{
  GM_SCENEOBJECT(PhysicalController)

  private:

    GMlib::PSphere<float>*   _head;
    GMlib::PCylinder<float>* _neck;
    GMlib::PCylinder<float>* _pointBody;
    GMlib::PCylinder<float>* _leftHand;
    GMlib::PCylinder<float>* _rightHand;
    GMlib::PCylinder<float>* _leftLeg;
    GMlib::PCylinder<float>* _rightLeg;
    GMlib::PCylinder<float>* _body;
    Bullet*                  _bullet;
    Bullet*                   bullet;
    visualizer*              _viz;
    visualizer*              _viz1;
    geometryshader*          shader;
    geometryshader*         _shader;





public:

  PhysicalController();
  void insertObject();
  void createShape1();
  void createShape2();

  void moveLeftHand();
  void moveRightHand();
  void moveleft();
  void moveright();
  void moveforward();
  void movebackward();

  //void insertbullet();
  //void createbullet();
  void shootbullet();


};


#endif
