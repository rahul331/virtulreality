#include "plane.h"

/*===================================================
 * this is the function to get the point which create the plane
=====================================================*/
GMlib::Point<float,3>& Plane::getCornerPoint() {
  return this->_pt;
}

/*===================================================
 * this function is the destruction function of the plane
=====================================================*/
Plane::~Plane(){

}


