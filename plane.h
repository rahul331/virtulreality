/*
 * This class create the wall class
 *  a wall create by one point and two vector
 */

#ifndef PLANE_H
#define PLANE_H


#include <QTimerEvent>
#include <QRectF>
#include <QMouseEvent>
#include <QDebug>
#include <stdexcept>
#include <thread>
#include <mutex>
#include <parametrics/gmpplane>
#include <parametrics/gmpbeziersurf>

class Plane: public GMlib::PPlane<float>{

public:
  /*===========================
   * this function is the construction function, this function
   * inherit from the PPlane class in GMlib and it can
   * use the default varriable of PPlane to use
  =============================*/
  using PPlane::PPlane;
  ~Plane();

  GMlib::Point<float,3>& getCornerPoint();

};

#endif
