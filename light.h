#ifndef LIGHT_H
#define LIGHT_H

#include <parametrics/gmpsurfvisualizer>

// gmlib
#include <opengl/bufferobjects/gmvertexbufferobject.h>
#include <opengl/bufferobjects/gmindexbufferobject.h>
#include <opengl/bufferobjects/gmuniformbufferobject.h>
#include <opengl/gmtexture.h>
#include <opengl/gmprogram.h>
#include <opengl/shaders/gmvertexshader.h>
#include <opengl/shaders/gmfragmentshader.h>
#include <scene/render/gmdefaultrenderer.h>

using namespace GMlib;

class light: public PSurfVisualizer<float,3>{
    GM_VISUALIZER(light)
public:
    light();

    light( const light& copy );

    void          render( const SceneObject* obj, const DefaultRenderer* renderer ) const;
    void          renderGeometry( const SceneObject* obj, const Renderer* renderer, const Color& color ) const;

    virtual void  replot( const DMatrix< DMatrix< Vector<float,3> > >& p,
                          const DMatrix< Vector<float,3> >& normals,
                          int m1, int m2, int d1, int d2,
                          bool closed_u, bool closed_v
    );

  private:
    GL::Program                 _prog;
    GL::Program                 _color_prog;

    GL::VertexBufferObject      _vbo;
    GL::IndexBufferObject       _ibo;
    GL::Texture                 _nmap;

    GLuint                      _no_strips;
    GLuint                      _no_strip_indices;
    GLsizei                     _strip_size;
    GLuint                       land;



    void                        draw() const;
    void                        initShaderProgram();

};

#endif // LIGHT_H
