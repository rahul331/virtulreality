#include "physicalcontroller.h"

/*=====================================================
 this function define the object to render to the scene
 to make the robot can not seperate, make one body as a center, then move the center
======================================================*/
void PhysicalController::insertObject(){

  GMlib::Point<float,3> point1(-20,-20,-20);
  GMlib::Vector<float,3> vec1(40,0,0);
  GMlib::Vector<float,3> vec2(0,40,0);


    Plane *plane1 = new Plane(point1,vec1,vec2);
    //plane1->toggleDefaultVisualizer();
   visualizer* _viz1 = new visualizer();
    plane1->insertVisualizer(_viz1);
    plane1->replot(20,20,1,1);
    this->insert(plane1);
    createShape1();
}


 void PhysicalController::createShape2(){
  //create square head
  GMlib::Point<float,3> pointFace(0,0,0);
  GMlib::Vector<float,3> vecFace1(0,10,0);
  GMlib::Vector<float,3> vecFace2(10,0,0);
  Plane* _face = new Plane(pointFace,vecFace1,vecFace2);
  _face->toggleDefaultVisualizer();
  _face->replot(20,20,1,1);
  this->insert(_face);



//create body...........................................................
  GMlib::Point<float,3> pointBody(-4,-22.5,0);
  GMlib::Vector<float,3> vecBody1(0,20,0);
  GMlib::Vector<float,3> vecBody2(20,0,0);
  Plane* _body = new Plane(pointBody,vecBody1,vecBody2);
  _body->toggleDefaultVisualizer();
  _body->replot(20,20,1,1);
   this->insert(_body);

//create left hand....................................................
  GMlib::Point<float,3> pointLeftHand(-6,-8,-2);
  _leftHand = new GMlib::PCylinder<float>(0.5,0,10);
  _leftHand->toggleDefaultVisualizer();
  _leftHand->replot(20,20,1,1);
  _leftHand->translate(pointLeftHand);
  _leftHand->rotate(30,GMlib::Point<float,3>(0,5,0));
  this->insert(_leftHand);

//create right hand....................................................
  GMlib::Point<float,3> pointRightHand(16,-14,5);
  auto _rightHand = new GMlib::PCylinder<float>(0.5,0,10);
  _rightHand->toggleDefaultVisualizer();
  _rightHand->replot(20,20,1,1);
  _rightHand->translate(pointRightHand);
  _rightHand->rotate(-30,GMlib::Point<float,3>(0,5,0));
  this->insert(_rightHand);

//create the left leg...............................................
  GMlib::Point<float,3> pointLeftLeg(0,-29,0);
  auto _leftLeg = new GMlib::PCylinder<float>(1,0,20);
  _leftLeg->toggleDefaultVisualizer();
  _leftLeg->replot(20,20,1,1);
  _leftLeg->translate(pointLeftLeg);
  this->insert(_leftLeg);


//create the right leg.....................................................
  GMlib::Point<float,3> pointRightLeg(10,-29,0);
  auto _rightLeg = new GMlib::PCylinder<float>(1,0,20);
  _rightLeg->toggleDefaultVisualizer();
  _rightLeg->replot(20,20,1,1);
  _rightLeg->translate(pointRightLeg);
  this->insert(_rightLeg);

}


void PhysicalController::createShape1(){

  GMlib::Point<float,3> pointBody(1,-2,1);
  _body = new GMlib::PCylinder<float>(2,0,6);
  _body->toggleDefaultVisualizer();
  _body->replot(20,20,1,1);
  _body->setMaterial(GMlib::GMmaterial::PolishedSilver);
  _body->translate(pointBody);
  this->insert(_body);

  GMlib::Point<float,3> pointNeck(0,3,1);
  _neck = new GMlib::PCylinder<float>(0.5,0,3);
  _neck->toggleDefaultVisualizer();
  _neck->replot(20,20,1,1);//sub division of object
  _neck->translate(pointNeck);
  _neck->setMaterial(GMlib::GMmaterial::PolishedSilver);
  _body->insert(_neck);

  GMlib::Point<float,3> pointHead(0,7,1);
  _head = new GMlib::PSphere<float>(2);
  _head->toggleDefaultVisualizer();
  _head->replot(20,20,1,1);
  _head->translate(pointHead);
  _head->setMaterial(GMlib::GMmaterial::BlackPlastic);
  _body->insert(_head);


  GMlib::Point<float,3> pointLeftHand(-3,7,-7);
  _leftHand = new GMlib::PCylinder<float>(1,0,3);
  _leftHand->toggleDefaultVisualizer();
  _leftHand->replot(20,20,1,1);
  _leftHand->translate(pointLeftHand);
  _leftHand->rotate(30,GMlib::Point<float,3>(0,5,0));
  _leftHand->setMaterial(GMlib::GMmaterial::PolishedSilver);
  _body->insert(_leftHand);

  GMlib::Point<float,3> pointRightHand(3.5,7,-7);
  _rightHand = new GMlib::PCylinder<float>(1,0,3);
  _rightHand->toggleDefaultVisualizer();
  _rightHand->replot(20,20,1,1);
  _rightHand->translate(pointRightHand);
  _rightHand->rotate(-30,GMlib::Point<float,3>(0,5,0));
  _rightHand->setMaterial(GMlib::GMmaterial::PolishedSilver);
  _body->insert(_rightHand);


  GMlib::Point<float,3> pointLeftLeg(-1.5,-1.7,-4);
  _leftLeg = new GMlib::PCylinder<float>(1,0,6);
  //_leftLeg->toggleDefaultVisualizer();

 geometryshader* shader = new geometryshader();
 _leftLeg->insertVisualizer(shader);

  _leftLeg->replot(20,20,1,1);
  _leftLeg->translate(pointLeftLeg);
  _leftLeg->setMaterial(GMlib::GMmaterial::PolishedSilver);
  _body->insert(_leftLeg);

  GMlib::Point<float,3> pointrightLeg(2,-1.7,-4);
  _rightLeg = new GMlib::PCylinder<float>(1,0,6);
  //_rightLeg->toggleDefaultVisualizer();

   geometryshader* _shader = new geometryshader();
  _rightLeg->insertVisualizer(_shader);
  _rightLeg->replot(20,20,1,1);
  _rightLeg->translate(pointrightLeg);
  _rightLeg->setMaterial(GMlib::GMmaterial::PolishedSilver);
  _body->insert(_rightLeg);

}

/*============================================================================
 * this is the construction function to insert the new object to the scene
=============================================================================*/
PhysicalController::PhysicalController(){
  insertObject();
}

    void PhysicalController::moveLeftHand(){
   _leftHand->rotate(60,GMlib::Point<float,3>(0,5,0));
}


    void PhysicalController::moveRightHand(){
   _rightHand->rotate(-60,GMlib::Point<float,3>(0,5,0));
}

    void PhysicalController::moveleft(){
   _body->translate(GMlib::Vector<float,3>(2,0,0));
  //_body->translate(60,GMlib::Point<float,3> (10,0,0));
}
    void PhysicalController::moveright(){
   _body->translate(GMlib::Vector<float,3>(-2,0,0));
}
    void PhysicalController::moveforward(){
   _body->translate(GMlib::Vector<float,3>(0,-2,0));
}
    void PhysicalController::movebackward(){
   _body->translate(GMlib::Vector<float,3>(0,2,0));
}

    void PhysicalController::shootbullet(){

        GMlib::Point<float,3> pointbullet(2,-1.7,-4);
        _bullet = new Bullet;
        _bullet->toggleDefaultVisualizer();
        _bullet->replot(20,20,1,1);
        _bullet->translate(pointbullet);
        _bullet->setMaterial(GMlib::GMmaterial::Ruby);
        _leftHand->insert(_bullet);

        bullet = new Bullet;
        bullet->toggleDefaultVisualizer();
        bullet->replot(20,20,1,1);
        bullet->translate(pointbullet);
        bullet->setMaterial(GMlib::GMmaterial::Ruby);
        _rightHand->insert(bullet);

    }





